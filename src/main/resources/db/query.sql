-- addNewMessage=
INSERT INTO messages (user_id, `body`, created_at, message_status_id)
VALUES (?, ?, NOW(), ?);

-- getUserIdByName=
SELECT u.id
FROM users AS u
WHERE u.username = ?;

-- getLastMessages=
SELECT m.id, m.user_id, m.created_at, m.message_status_id
FROM messages AS m
ORDER BY id
DESC limit ?;

-- getUserNameById=
SELECT u.username
FROM users AS u
WHERE u.id = ?;

-- getTitleStatusById=
SELECT ms.title
FROM message_statuses AS ms
WHERE ms.id = ?;

-- getLastId=
SELECT u.id
FROM users AS u
ORDER BY u.id
DESC LIMIT 1;

-- isLogged=
SELECT message_status_id != 4 AS isLogged
FROM messages AS m
WHERE m.user_id = ?
ORDER BY created_at
DESC LIMIT 1;

-- unKick=
delete
FROM messages
WHERE body = ?
 AND message_status_id = 3;

-- getRole=
SELECT r.title
FROM roles AS r
INNER JOIN users AS u
WHERE u.role_id = r.id
 AND u.id = ?;

-- isKicked=
SELECT message_status_id = 3 AS isKicked
FROM messages AS m
WHERE m.body = ?
ORDER BY created_at
DESC LIMIT 1;

-- createUser=
INSERT INTO users (username, role_id)
VALUES (?, ?);

-- getAllLogged=
SELECT t1.user_id
FROM messages t1
LEFT JOIN messages t2
on
(t1.user_id = t2.user_id
 AND t1.created_at < t2.created_at
 AND t2.message_status_id = 4)
WHERE t1.message_status_id = 1
 AND t2.created_at is null
ORDER BY t1.user_id;

-- getAllKicked=
SELECT u.id
from users AS u
INNER JOIN messages AS m
WHERE m.message_status_id = 3
 AND m.body = u.id
ORDER BY m.user_id;

-- getUserById=
SELECT u.username, r.title
FROM users AS u
INNER JOIN roles AS r
WHERE u.role_id = r.id
 AND u.id = ?;