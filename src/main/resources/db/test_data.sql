use chat;
INSERT
INTO roles(title, description) values ('admin', 'Admin role');
INSERT
INTO roles(id, title, description)
VALUES ('user', 'User role');

INSERT
INTO users(username, role_id)
VALUES ('Denis', 1);
INSERT
INTO users(username, role_id)
VALUES ('Egor', 2);
INSERT
INTO users(username, role_id)
VALUES ('Vlad', 2);
INSERT
INTO users(username, role_id)
VALUES ('Dima', 2);

INSERT
INTO message_statuses(title, description)
VALUES ('LOGIN', 'User logged into chat');
INSERT
INTO message_statuses(title, description)
VALUES ('MESSAGE', 'User left a message');
INSERT
INTO message_statuses(title, description)
VALUES ('KICK', 'The user threw another user out of the chat');
INSERT
INTO message_statuses(title, description)
VALUES ('LOGOUT', 'The user has left the chat');

INSERT
INTO messages(user_id, body, created_at, message_status_id)
VALUES (5, '', NOW(), 1);
INSERT
INTO messages(user_id, body, created_at, message_status_id)
VALUES (6, '', NOW(), 1);
INSERT
INTO messages(user_id, body, created_at, message_status_id)
VALUES (7, '', NOW(), 1);
INSERT
INTO messages(user_id, body, created_at, message_status_id)
VALUES (8, '', NOW(), 1);
INSERT
INTO messages(user_id, body, created_at, message_status_id)
VALUES (8, '', NOW(), 4);