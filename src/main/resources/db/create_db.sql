CREATE database IF NOT EXISTS chat;
USE chat;
CREATE table IF NOT EXISTS roles
(
    id          INT          NOT NULL primary key auto_increment,
    title       varchar(30)  NOT NULL UNIQUE,
    description varchar(100) NOT NULL
);
CREATE table IF NOT EXISTS users
(
    id       int         NOT NULL primary key auto_increment,
    username varchar(30) NOT NULL UNIQUE,
    role_id  int         NOT NULL,
    constraint `fk_users_to_roles` foreign key (role_id)
        references roles (id)
        ON delete NO ACTION
        ON update NO ACTION
);
CREATE table IF NOT EXISTS message_statuses
(
    id          INT          NOT NULL primary key auto_increment,
    title       varchar(15)  NOT NULL UNIQUE,
    description varchar(100) NOT NULL
);
CREATE table IF NOT EXISTS messages
(
    id                int       NOT NULL primary key auto_increment,
    user_id           int       NOT NULL,
    body              text,
    created_at        timestamp NOT NULL default now(),
    message_status_id int       NOT NULL,
    constraint `fk_messages_to_users` foreign key (user_id)
        references users (id)
        ON delete NO ACTION
        ON update NO ACTION,
    constraint `fk_messages_to_message_statuses` foreign key (message_status_id)
        references message_statuses (id)
        ON delete NO ACTION
        ON update NO ACTION
);
alter table messages add index fk_messages_to_users (user_id);
alter table messages add index fk_messages_to_statuses (message_status_id);
alter table users add index fk_users_to_roles (role_id);
