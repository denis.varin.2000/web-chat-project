package com.epam.chat.datalayer.db;

import com.epam.chat.datalayer.UserDAO;
import com.epam.chat.datalayer.connectionpool.ConnectionPool;
import com.epam.chat.datalayer.connectionpool.ConnectionPoolException;
import com.epam.chat.datalayer.dto.Role;
import com.epam.chat.datalayer.dto.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MySqlUserDAO implements UserDAO {
    public static final String KEY_USERNAME = "username";
    private static final String CONNECTION_TO_DATABASE_ERROR = "Connection to database is error";
    private static final int PARAMETER_INDEX_ONE = 1;
    private static final int PARAMETER_INDEX_TWO = 2;
    private static final int PARAMETER_INDEX_THREE = 3;
    private static final String KEY_ADD_NEW_MESSAGE = "addNewMessage";
    private static final String KEY_GET_LAST_ID = "getLastId";
    private static final String KEY_ID = "id";
    private static final String KEY_GET_USER_ID = "getUserIdByName";
    private static final String KEY_IS_LOGGED = "isLogged";
    private static final String GOOD_BYE_CHAT = "GoodBye Chat";
    private static final int KEY_STATUS_ID = 1;
    private static final int STATUS_KICK = 3;
    private static final int KEY_LOGOUT_STATUS = 4;
    private static final String KEY_UN_KICK = "unKick";
    private static final String KEY_GET_ROLE = "getRole";
    private static final String KEY_TITLE = "title";
    private static final String KEY_IS_KICKED = "isKicked";
    private static final String HELLO_CHAT = "Hello Chat";
    private static final String KEY_CREATE_USER = "createUser";
    private static final String KEY_GET_ALL_LOGGED = "getAllLogged";
    private static final String KEY_GET_USER_BY_ID = "getUserById";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_GET_ALL_KICKED = "getAllKicked";
    private final ConnectionPool connectionPool;
    private Statement statement;
    private ResultSet resultSet;
    private SQLQueryHolder SQLQueryHolder;

    public MySqlUserDAO(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
        this.SQLQueryHolder = new SQLQueryHolder();
    }

    /**
     * @param userToLogin user we want to login
     */
    @Override
    public void login(User userToLogin) {
        if (!isLoggedIn(userToLogin) && !isKicked(userToLogin)) {
            String nick = userToLogin.getName();
            int userId = getLastId() + 1;
            int roleID = userToLogin.getRole().getId();
            try (Connection connection = connectionPool.takeConnection()) {
                connection.setAutoCommit(false);
                try (PreparedStatement preparedStatement =
                             connection.prepareStatement(SQLQueryHolder.getValue(KEY_ADD_NEW_MESSAGE))) {
                    preparedStatement.setInt(PARAMETER_INDEX_ONE, userId);
                    preparedStatement.setString(PARAMETER_INDEX_TWO, HELLO_CHAT);
                    preparedStatement.setInt(PARAMETER_INDEX_THREE, KEY_STATUS_ID);
                    createUser(connection, nick, roleID);
                    preparedStatement.execute();
                    connection.commit();
                    connection.setAutoCommit(true);
                }
            } catch (SQLException | ConnectionPoolException e) {
                throw new ChatException(CONNECTION_TO_DATABASE_ERROR, e);
            }
        }
    }

    /**
     * @param user user to check
     * @return
     */
    @Override
    public boolean isLoggedIn(User user) {
        boolean result = false;
        int userId = getUserIdByNick(user.getName());
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQLQueryHolder.getValue(KEY_IS_LOGGED))) {
            preparedStatement.setInt(PARAMETER_INDEX_ONE, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                result = resultSet.getBoolean(KEY_IS_LOGGED);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new ChatException(CONNECTION_TO_DATABASE_ERROR, e);
        }
        return result;
    }

    /**
     * @param userToLogout user we want to logout
     */
    @Override
    public void logout(User userToLogout) {
        int userId = getUserIdByNick(userToLogout.getName());
        addNewMessage(userId, GOOD_BYE_CHAT, KEY_LOGOUT_STATUS);
    }

    @Override
    public void unkick(User user) {
        int userId = getUserIdByNick(user.getName());
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQLQueryHolder.getValue(KEY_UN_KICK))) {
            preparedStatement.setInt(PARAMETER_INDEX_ONE, userId);
            preparedStatement.execute();
        } catch (SQLException | ConnectionPoolException e) {
            throw new ChatException(CONNECTION_TO_DATABASE_ERROR, e);
        }
    }

    /**
     * @param admin        - user responsible for the kick action (with the role admin)
     * @param kickableUser - user that should be kicked
     */
    @Override
    public void kick(User admin, User kickableUser) {
        if (!admin.getRole().isGetRightToKick()) {
            return;
        }
        int adminId = getUserIdByNick(admin.getName());
        int userId = getUserIdByNick(kickableUser.getName());
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQLQueryHolder.getValue(KEY_ADD_NEW_MESSAGE))) {
            connection.setAutoCommit(false);
            preparedStatement.setInt(PARAMETER_INDEX_ONE, adminId);
            preparedStatement.setString(PARAMETER_INDEX_TWO, String.valueOf(userId));
            preparedStatement.setInt(PARAMETER_INDEX_THREE, STATUS_KICK);
            preparedStatement.execute();
            PreparedStatement preparedStatementLogout =
                    connection.prepareStatement(SQLQueryHolder.getValue(KEY_ADD_NEW_MESSAGE));
            preparedStatementLogout.setInt(PARAMETER_INDEX_ONE, userId);
            preparedStatementLogout.setString(PARAMETER_INDEX_TWO, GOOD_BYE_CHAT);
            preparedStatementLogout.setInt(PARAMETER_INDEX_THREE, KEY_LOGOUT_STATUS);
            preparedStatementLogout.execute();
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException | ConnectionPoolException e) {
            throw new ChatException(CONNECTION_TO_DATABASE_ERROR, e);
        }

    }

    /**
     * @param user user to check
     * @return
     */
    @Override
    public boolean isKicked(User user) {
        boolean result = false;
        int userId = getUserIdByNick(user.getName());
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQLQueryHolder.getValue(KEY_IS_KICKED))) {
            preparedStatement.setInt(PARAMETER_INDEX_ONE, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                result = resultSet.getBoolean(KEY_IS_KICKED);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new ChatException(CONNECTION_TO_DATABASE_ERROR, e);
        }
        return result;
    }


    /**
     * @return
     */
    @Override
    public List<User> getAllLogged() {
        List<User> userList = new ArrayList<>();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQLQueryHolder.getValue(KEY_GET_ALL_LOGGED))) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userList.add(getUserById(connection, resultSet.getInt(KEY_USER_ID)));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new ChatException(CONNECTION_TO_DATABASE_ERROR, e);
        }
        return userList;
    }


    /**
     * @return
     */
    @Override
    public List<User> getAllKicked() {
        List<User> userKickedList = new ArrayList<>();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQLQueryHolder.getValue(KEY_GET_ALL_KICKED))) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userKickedList.add(getUserById(connection, resultSet.getInt(KEY_ID)));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new ChatException(CONNECTION_TO_DATABASE_ERROR, e);
        }
        return userKickedList;
    }

    /**
     * @param nick nick of user to find the role
     * @return
     */
    @Override
    public Role getRole(String nick) {
        int userId = getUserIdByNick(nick);
        Role role = null;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQLQueryHolder.getValue(KEY_GET_ROLE))) {
            preparedStatement.setInt(PARAMETER_INDEX_ONE, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                role = Role.valueOf(resultSet.getString(KEY_TITLE).toUpperCase(Locale.ROOT));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new ChatException(CONNECTION_TO_DATABASE_ERROR, e);
        }
        return role;
    }

    private User getUserById(Connection connection, int id) throws SQLException {
        User user = null;
        Role role;
        String name;
        PreparedStatement preparedStatement =
                connection.prepareStatement(SQLQueryHolder.getValue(KEY_GET_USER_BY_ID));
        preparedStatement.setInt(PARAMETER_INDEX_ONE, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            name = resultSet.getString(KEY_USERNAME);
            role = Role.valueOf(resultSet.getString(KEY_TITLE).toUpperCase(Locale.ROOT));
            user = new User(name, role);
        }
        return user;
    }

    private void addNewMessage(int userId, String body, int status) {
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQLQueryHolder.getValue(KEY_ADD_NEW_MESSAGE))) {
            preparedStatement.setInt(PARAMETER_INDEX_ONE, userId);
            preparedStatement.setString(PARAMETER_INDEX_TWO, body);
            preparedStatement.setInt(PARAMETER_INDEX_THREE, status);
            preparedStatement.execute();
        } catch (SQLException | ConnectionPoolException e) {
            throw new ChatException(CONNECTION_TO_DATABASE_ERROR, e);
        }
    }

    private int getLastId() {
        int result = 0;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQLQueryHolder.getValue(KEY_GET_LAST_ID))) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                result = resultSet.getInt(KEY_ID);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new ChatException(CONNECTION_TO_DATABASE_ERROR, e);
        }
        return result;
    }

    private int getUserIdByNick(String userName) {
        int result = 0;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQLQueryHolder.getValue(KEY_GET_USER_ID))) {
            preparedStatement.setString(PARAMETER_INDEX_ONE, userName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                result = Integer.parseInt(resultSet.getString(KEY_ID));
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new ChatException(CONNECTION_TO_DATABASE_ERROR, e);
        }
        return result;
    }

    private void createUser(Connection connection, String nick, int roleID) throws SQLException {
        PreparedStatement preparedStatement =
                connection.prepareStatement(SQLQueryHolder.getValue(KEY_CREATE_USER));
        preparedStatement.setString(PARAMETER_INDEX_ONE, nick);
        preparedStatement.setInt(PARAMETER_INDEX_TWO, roleID);
        preparedStatement.execute();
    }
}