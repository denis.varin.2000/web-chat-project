package com.epam.chat.datalayer.xml;

import org.junit.Test;

public class XMLHolderTest {
    @Test
    public void successfulValidation() {
        String schemaName = "chat.xsd";
        String xmlName = "chat.xml";

        new XMLHolder(schemaName, xmlName);
    }

    @Test(expected = RuntimeException.class)
    public void wrongXML() {
        String schemaName = "chat.xsd";
        String xmlName = "messageWithError.xml";

        new XMLHolder(schemaName, xmlName);
    }
}